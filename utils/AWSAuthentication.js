import AWS from 'aws-sdk';
import utils from './utils';
import {
    AuthenticationDetails,
    CognitoUser,
    CognitoUserPool
} from "../libs/aws-cognito-identity/index";

let instance = null;

export default class AWSAuthentication {

    constructor() {
        if(instance){
            return instance;
        } else {
            instance = this;
            this._user = null;
            this._isAuthedUser = false;
            this._identityPoolId = 'us-east-1:3b74a6ca-df79-4a61-814e-58f98c65906e';
            this._region = 'us-east-1';
            this._bucket = 'steno-audio-files';
            this._cognito = {
                UserPoolId: 'us-east-1_WHP5AMIlb',
                ClientId: '53gvggj2g8sg9p9h4jv6unhaiv'
            };
            this._s3 = null;
            this._cognitoIdentityProvider = null;
            this.initAWS();
            this.getCredentials();
        }

        return instance;
    }

    get currentUser() {
        return this._user;
    }

    get identityPoolId() {
        return this._identityPoolId;
    }

    set identityPoolId(value) {
        this._identityPoolId = value;
        this.updateAWS({
            credentials: new AWS.CognitoIdentityCredentials({
                IdentityPoolId: this._identityPoolId
            })
        })
    }

    get region() {
        return this._region;
    }

    set region(value) {
        this._region = value;
        this.updateAWS({
            region: this._region
        })
    }

    get bucket() {
        return this._bucket;
    }

    set bucket(value) {
        this._bucket = value;
    }

    initAWS() {
        // Set the region where the identity pool exists (us-east-1, eu-west-1)
        AWS.config.region = this._region;
        // Configure the credentials provider to use your identity pool
        AWS.config.credentials = new AWS.CognitoIdentityCredentials({
            IdentityPoolId: this._identityPoolId
        });
        this._s3 = new AWS.S3({
            signatureVersion: 'v4'
        });
        this._cognitoIdentityProvider = new AWS.CognitoIdentityServiceProvider();
    }

    updateAWS(params) {
        AWS.config.update(params);
    }

    getCredentials() {
        // Make the call to obtain credentials
        //AWS.config.credentials.get( async () => {
        //    console.log('Credentials Obtained...');
        //});
    }

    onRegister(name, userName, password, callback) {
        let params = {
            ClientId: this._cognito.ClientId,
            Password: password,
            Username: userName,
            UserAttributes: [
                {Name: 'email',Value: userName},
                {Name: 'name',Value: name}
            ]
        };
        this._cognitoIdentityProvider.signUp(params, (err, result) => {
            if (err) {
                console.log('onRegister', err);
                callback(err);
            } else {
                console.log('onRegister', result);
                callback(null);
            }
        });
    }

    onVerify(userName, code, callback) {
        let params = {
            ClientId: this._cognito.ClientId,
            ConfirmationCode: code,
            ForceAliasCreation: true,
            Username: userName
        };
        this._cognitoIdentityProvider.confirmSignUp(params, (err,result) => {
            if (err) {
                console.log('onVerify', err);
                callback(err);
            } else {
                console.log('onVerify', result);
                callback(null);
            }
        });
    }

    onReset(userName, callback) {
        let params = {
            ClientId: this._cognito.ClientId,
            Username: userName
        };
        this._cognitoIdentityProvider.forgotPassword(params, (err,result) => {
            if (err) {
                console.log('onReset', err);
                callback(err);
            } else {
                console.log('onReset', result);
                callback(null);
            }
        });
    }

    onChangePassword(userName, code, password, callback) {
        let params = {
            ClientId: this._cognito.ClientId,
            ConfirmationCode: code,
            Password: password,
            Username: userName
        };
        this._cognitoIdentityProvider.confirmForgotPassword(params, (err,result) => {
            if (err) {
                console.log('onChangePassword', err);
                callback(err);
            } else {
                console.log('onChangePassword', result);
                callback(null);
            }
        });
    }

    onSignIn(userName, password, callback){
        const userPool = new CognitoUserPool(this._cognito);
        const authDetails = new AuthenticationDetails({
            Username: userName,
            Password: password
        });
        const cognitoUser = new CognitoUser({
            Username: userName,
            Pool: userPool
        });

        const start = new Date().getTime();
        cognitoUser.authenticateUser(authDetails, {
            onSuccess: (result) => {
                console.log('authenticateUser time', new Date().getTime() - start);
                const jwt = utils.parseJwt(result.getAccessToken().getJwtToken());
                utils.setEmailUserID(userName, jwt.sub, () => {
                    callback(null)
                });
            },
            onFailure: (err) => {
                callback(err)
            }
        });
    }

    getPreSignedUrl( fileName, prefix = 'uploads/', expiration = 60*30, callback = null) {
        this._s3.getSignedUrl('putObject', {
            Bucket: this._bucket,
            Key: prefix+fileName,
            Expires: expiration,
            ACL: 'public-read'
        }, (err, presignedUrl) => {
            if(callback !== null ) {
                callback(err, presignedUrl);
            } else {
                if(err !== null) {
                    return presignedUrl
                }
                return err;
            }
        });
    }

}