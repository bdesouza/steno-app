import { AsyncStorage } from "react-native";
import {util} from "aws-sdk/dist/aws-sdk-react-native";

const EMAIL = 'EMAIL';
const USER_ID = 'USER_ID';

exports.isSignedIn = (callback) => {
    AsyncStorage.getItem('USER_ID', (err, result) => {
        callback(err, result);
    });
};

exports.setEmailUserID = (email, userId, callback) => {
    AsyncStorage.setItem(EMAIL, email, () => {
        AsyncStorage.setItem(USER_ID, userId, () => {
            callback();
        });
    });
};

exports.getEmailUserID = (callback) => {
    AsyncStorage.getItem(EMAIL, (err, email) => {
        AsyncStorage.getItem(USER_ID, (err, userId) => {
            callback(err, {email: email, userId: userId});
        });
    });
};

exports.removeEmailUserID = (callback) => {
    AsyncStorage.removeItem(EMAIL, () => {
        AsyncStorage.removeItem(USER_ID, () => {
            callback();
        });
    });
};

exports.parseJwt = (token) => {
    const payload = token.split('.')[1];
    return JSON.parse(util.base64.decode(payload).toString('utf8'));
};
