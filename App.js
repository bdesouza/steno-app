import React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import { AppLoading, Asset, Font } from 'expo';
import AWSAuthentication from './utils/AWSAuthentication'
import PrimaryNav from './navigation/Navigation';
import {isSignedIn} from './utils/utils';

export default class App extends React.Component {
    state = {
        assetsAreLoaded: false,
        signedIn: false,
        pendingAuthCheck: true
    };

    componentWillMount() {
        this._loadAssetsAsync();
        this.aws = new AWSAuthentication();
    }

    componentDidMount() {
        isSignedIn((err, result) => {
            this.setState({signedIn: result, pendingAuthCheck: false})
        })
    }

    _loadAssetsAsync = async () => {
        try {
            await Promise.all([
                Asset.loadAsync([
                    //require('./assets/images/robot-dev.png'),
                    //require('./assets/images/robot-prod.png'),
                ]),
                Font.loadAsync([
                    // This is the font that we are using for our tab bar
                    { 'dripicons': require('./assets/fonts/Dripicons.ttf') },
                ]),
            ]);
        } catch (e) {
            // In this case, you might want to report the error to your error
            // reporting service, for example Sentry
            console.warn(
                'There was an error caching assets (see: App.js), perhaps due to a ' +
                'network timeout, so we skipped caching. Reload the app to try again.'
            );
            console.log(e);
        } finally {
            this.setState({ assetsAreLoaded: true });
        }
    }


    render() {
        if (!this.state.assetsAreLoaded && !this.props.skipLoadingScreen && !this.state.pendingAuthCheck) {
            return <AppLoading />;
        } else {
            let Navigation = PrimaryNav(this.state.signedIn);

            return (
                <View style={styles.container}>
                    {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
                    {Platform.OS === 'android' && <View style={styles.statusBarUnderlay} />}
                  <Navigation />
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    statusBarUnderlay: {
        height: 24,
        backgroundColor: 'rgba(0,0,0,0.2)'
    },
});