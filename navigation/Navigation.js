import {StackNavigator} from "react-navigation";
import { StatusBar } from 'react-native';
import { Colors } from '../constants/Colors';

import Home from "../screens/Home";
import History from "../screens/History";
import Settings from "../screens/Settings";

import SignInView from '../screens/authentication/SignInView';
import SignOutView from '../screens/authentication/SignOutView';
import RegisterView from '../screens/authentication/RegisterView';
import VerifyView from '../screens/authentication/VerifyView';
import ResetPasswordView from "../screens/authentication/ResetPasswordView";
import ChangePasswordView from "../screens/authentication/ChangePasswordView";

const PrimaryNav = (signedIn) => {
    return StackNavigator({
        Home: {screen: Home, navigationOptions:{header:null}},
        History: {screen: History, navigationOptions:{title:'History'}},
        Settings: {screen: Settings, navigationOptions:{title:'Settings'}},
        SignIn: {screen: SignInView, navigationOptions:{header:null}},
        SignOut: {screen: SignOutView, navigationOptions:{header:null}},
        Register: {screen: RegisterView, navigationOptions:{header:null}},
        Verify: {screen: VerifyView, navigationOptions:{header:null}},
        Reset: {screen: ResetPasswordView, navigationOptions:{header:null}},
        ChangePassword: {screen: ChangePasswordView, navigationOptions:{header:null}},
    }, {
        // Default config for all screens
        headerMode: 'screen',
        initialRouteName: signedIn ? 'Home' : 'SignIn'
    });
};


export default PrimaryNav