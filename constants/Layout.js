import {Dimensions} from "react-native";

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const wp = (percentage) => {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
};

export const AppDimension = {
        width: viewportWidth,
        height: viewportHeight,
        wp: wp
};
