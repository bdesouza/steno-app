import {createIconSet} from "@expo/vector-icons";

const glyphMap = {
    'icon-alarm': "a",
    'icon-align-center': "b",
    'icon-align-justify': "c",
    'icon-align-left': "d",
    'icon-align-right': "e",
    'icon-anchor': "f",
    'icon-archive': "g",
    'icon-arrow-down': "h",
    'icon-arrow-left': "i",
    'icon-arrow-right': "j",
    'icon-arrow-thin-down': "k",
    'icon-arrow-thin-left': "l",
    'icon-arrow-thin-right': "m",
    'icon-arrow-thin-up': "n",
    'icon-arrow-up': "o",
    'icon-article': "p",
    'icon-backspace': "q",
    'icon-basket': "r",
    'icon-basketball': "s",
    'icon-battery-empty': "t",
    'icon-battery-full': "u",
    'icon-battery-low': "v",
    'icon-battery-medium': "w",
    'icon-bell': "x",
    'icon-blog': "y",
    'icon-bluetooth': "z",
    'icon-bold': "A",
    'icon-bookmark': "B",
    'icon-bookmarks': "C",
    'icon-box': "D",
    'icon-briefcase': "E",
    'icon-brightness-low': "F",
    'icon-brightness-max': "G",
    'icon-brightness-medium': "H",
    'icon-broadcast': "I",
    'icon-browser': "J",
    'icon-browser-upload': "K",
    'icon-brush': "L",
    'icon-calendar': "M",
    'icon-camcorder': "N",
    'icon-camera': "O",
    'icon-card': "P",
    'icon-cart': "Q",
    'icon-checklist': "R",
    'icon-checkmark': "S",
    'icon-chevron-down': "T",
    'icon-chevron-left': "U",
    'icon-chevron-right': "V",
    'icon-chevron-up': "W",
    'icon-clipboard': "X",
    'icon-clock': "Y",
    'icon-clockwise': "Z",
    'icon-cloud': "0",
    'icon-cloud-download': "1",
    'icon-cloud-upload': "2",
    'icon-code': "3",
    'icon-contract': "4",
    'icon-contract-2': "5",
    'icon-conversation': "6",
    'icon-copy': "7",
    'icon-crop': "8",
    'icon-cross': "9",
    'icon-crosshair': "!",
    'icon-cutlery': "\"",
    'icon-device-desktop': "#",
    'icon-device-mobile': "$",
    'icon-device-tablet': "%",
    'icon-direction': "&",
    'icon-disc': "'",
    'icon-document': "(",
    'icon-document-delete': ")",
    'icon-document-edit': "*",
    'icon-document-new': "+",
    'icon-document-remove': ",",
    'icon-dot': "-",
    'icon-dots-2': ".",
    'icon-dots-3': "/",
    'icon-download': ":",
    'icon-duplicate': ";",
    'icon-enter': "<",
    'icon-exit': "=",
    'icon-expand': ">",
    'icon-expand-2': "?",
    'icon-experiment': "@",
    'icon-export': "[",
    'icon-feed': "]",
    'icon-flag': "^",
    'icon-flashlight': "_",
    'icon-folder': "`",
    'icon-folder-open': "{",
    'icon-forward': "|",
    'icon-gaming': "}",
    'icon-gear': "~",
    'icon-graduation': "\\",
    'icon-graph-bar': "\e000",
    'icon-graph-line': "\e001",
    'icon-graph-pie': "\e002",
    'icon-headset': "\e003",
    'icon-heart': "\e004",
    'icon-help': "\e005",
    'icon-home': "\e006",
    'icon-hourglass': "\e007",
    'icon-inbox': "\e008",
    'icon-information': "\e009",
    'icon-italic': "\e00a",
    'icon-jewel': "\e00b",
    'icon-lifting': "\e00c",
    'icon-lightbulb': "\e00d",
    'icon-link': "\e00e",
    'icon-link-broken': "\e00f",
    'icon-list': "\e010",
    'icon-loading': "\e011",
    'icon-location': "\e012",
    'icon-lock': "\e013",
    'icon-lock-open': "\e014",
    'icon-mail': "\e015",
    'icon-map': "\e016",
    'icon-media-loop': "\e017",
    'icon-media-next': "\e018",
    'icon-media-pause': "\e019",
    'icon-media-play': "\e01a",
    'icon-media-previous': "\e01b",
    'icon-media-record': "\e01c",
    'icon-media-shuffle': "\e01d",
    'icon-media-stop': "\e01e",
    'icon-medical': "\e01f",
    'icon-menu': "\e020",
    'icon-message': "\e021",
    'icon-meter': "\e022",
    'icon-microphone': "\e023",
    'icon-minus': "\e024",
    'icon-monitor': "\e025",
    'icon-move': "\e026",
    'icon-music': "\e027",
    'icon-network-1': "\e028",
    'icon-network-2': "\e029",
    'icon-network-3': "\e02a",
    'icon-network-4': "\e02b",
    'icon-network-5': "\e02c",
    'icon-pamphlet': "\e02d",
    'icon-paperclip': "\e02e",
    'icon-pencil': "\e02f",
    'icon-phone': "\e030",
    'icon-photo': "\e031",
    'icon-photo-group': "\e032",
    'icon-pill': "\e033",
    'icon-pin': "\e034",
    'icon-plus': "\e035",
    'icon-power': "\e036",
    'icon-preview': "\e037",
    'icon-print': "\e038",
    'icon-pulse': "\e039",
    'icon-question': "\e03a",
    'icon-reply': "\e03b",
    'icon-reply-all': "\e03c",
    'icon-return': "\e03d",
    'icon-retweet': "\e03e",
    'icon-rocket': "\e03f",
    'icon-scale': "\e040",
    'icon-search': "\e041",
    'icon-shopping-bag': "\e042",
    'icon-skip': "\e043",
    'icon-stack': "\e044",
    'icon-star': "\e045",
    'icon-stopwatch': "\e046",
    'icon-store': "\e047",
    'icon-suitcase': "\e048",
    'icon-swap': "\e049",
    'icon-tag': "\e04a",
    'icon-tag-delete': "\e04b",
    'icon-tags': "\e04c",
    'icon-thumbs-down': "\e04d",
    'icon-thumbs-up': "\e04e",
    'icon-ticket': "\e04f",
    'icon-time-reverse': "\e050",
    'icon-to-do': "\e051",
    'icon-toggles': "\e052",
    'icon-trash': "\e053",
    'icon-trophy': "\e054",
    'icon-upload': "\e055",
    'icon-user': "\e056",
    'icon-user-group': "\e057",
    'icon-user-id': "\e058",
    'icon-vibrate': "\e059",
    'icon-view-apps': "\e05a",
    'icon-view-list': "\e05b",
    'icon-view-list-large': "\e05c",
    'icon-view-thumb': "\e05d",
    'icon-volume-full': "\e05e",
    'icon-volume-low': "\e05f",
    'icon-volume-medium': "\e060",
    'icon-volume-off': "\e061",
    'icon-wallet': "\e062",
    'icon-warning': "\e063",
    'icon-web': "\e064",
    'icon-weight': "\e065",
    'icon-wifi': "\e066",
    'icon-wrong': "\e067",
    'icon-zoom-in': "\e068",
    'icon-zoom-out': "\e069"
};

for (let item in glyphMap) {
    if (glyphMap[item].indexOf('e') !== -1 && glyphMap[item].length > 1) {
        glyphMap[item] = parseInt(glyphMap[item], 16)
    }
}

const Icon = createIconSet(glyphMap, 'dripicons');

export default Icon