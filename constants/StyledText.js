import styled from 'styled-components/native';
import { Colors } from '../constants/Colors';


export const MonoText = styled.Text`
  font-family: space-mono;
`;

export const Text = styled.Text`
  font-family: avenir;
`;

export const LightText = Text.extend`
  font-family: avenir-light;
`;

export const BoldText = Text.extend`
  font-family: avenir-bold;
`;

export const LinkText = Text.extend`
  color: ${Colors.tintColor};
`;