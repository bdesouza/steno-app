const tintColor = '#f63932';
const textColor = '#fff';
const inactiveTintColor = '#ababab';

export const Colors = {
    errorText: textColor,
    warningText: '#666804',
    noticeText: textColor,
    errorBackground: tintColor,
    warningBackground: '#eaeb5e',
    noticeBackground: '#398ff7',
    tintColor,

    btnTextColor: textColor,
    btnColorPrimary: '#2196f3',
    dangerBtnColor: tintColor,

    appBackground: '#311b92',
    secondaryBackground: '#ffffff'
};