import React from "react";
import {View, Text, TouchableOpacity, Platform } from "react-native";
import styled from 'styled-components/native';
import { Colors } from '../constants/Colors';
import Icon from '../constants/StyledIcon';
import {isSignedIn} from '../utils/utils';
import { Card, ListItem, Button } from 'react-native-elements'

const MainView = styled.View`
  background: ${Colors.secondaryBackground};
  height: 100%;
  width: 100%;
`;

export default class SettingsScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            signedIn: false,
            pendingAuthCheck: true
        };

    }

    componentDidMount() {
        isSignedIn((err, result) => {
            this.setState({signedIn: result, pendingAuthCheck: false})
        })
    }

    _showSignOutButton = () => {
        return (

                <Button
                    onPress={() => {this.props.navigation.navigate('SignOut')}}
                    iconComponent={Icon}
                    icon={{name: 'icon-exit'}}
                    backgroundColor={Colors.dangerBtnColor}
                    title='Sign Out' />
        )
    };

    render() {
        return (
            <MainView>
                <Card title="">
                    {this.state.signedIn && this._showSignOutButton()}
                </Card>
            </MainView>
        );
    }
}