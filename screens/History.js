import React from "react";
import {View, Text, TouchableOpacity, Platform } from "react-native";
import AWSAuthentication from '../utils/AWSAuthentication'
import styled from 'styled-components/native';
import { Colors } from '../constants/Colors';
import Icon from '../constants/StyledIcon';

const MainView = styled.View`
  background: ${Colors.secondaryBackground};
  height: 100%;
  width: 100%;
`;

export default class HistoryScreen extends React.Component {

    constructor(props) {
        super(props);
        this.aws = new AWSAuthentication();
    }



    render() {
        return (
            <MainView>
            </MainView>
        );
    }
}