import React from 'react';
import SignInView from './SignInView';

class SignOutView extends SignInView {
    constructor(props) {
        props.signout = true;
        super(props)
    }
}

export default SignOutView