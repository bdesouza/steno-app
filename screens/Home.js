import React from "react";
import {View, Text, TouchableOpacity, Platform } from "react-native";
import { Permissions, Audio} from 'expo';
import AWSAuthentication from '../utils/AWSAuthentication'
import styled from 'styled-components/native';
import { Colors } from '../constants/Colors';
import Icon from '../constants/StyledIcon';

const MainView = styled.View`
  background: ${Colors.appBackground};
  height: 100%;
  width: 100%;
`;

const StyledRoundButton = styled.TouchableOpacity`
  background: ${Colors.btnColorPrimary};
  height: ${props => props.radius};
  width: ${props => props.radius};
  border-width:1;
  border-color: rgba(0,0,0,0.2);
  align-items:center;
  justify-content:center;
  border-radius:${props => props.radius};
  margin-left: 10;
  margin-right: 10;
  margin-top: 10;
  margin-bottom: 10;
`;

const StyledRoundButton2 = StyledRoundButton.extend`
  background-color: rgba(0,0,0,0);
  border-color: rgba(0,0,0,0);
`;

const RecordingCanvas = styled.View`
  flex:0.9;
  flex-direction: row;
  height: 100%;
  width: 100%;
  align-items:center;
  justify-content:center;
`;

const HeaderView = styled.View`
  flex:0.1;
  flex-direction: row;
  justify-content: space-between;
`;

const QUALITY = Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY;

export default class HomeScreen extends React.Component {

    constructor(props) {
        super(props);
        this.recording = null;
        this.state = {
            haveRecordingPermissions: false,
            isLoading: false,
            isPlaybackAllowed: false,
            muted: false,
            soundPosition: null,
            soundDuration: null,
            recordingDuration: null,
            shouldPlay: false,
            isPlaying: false,
            isRecording: false,
            fontLoaded: false,
            shouldCorrectPitch: true,
            volume: 1.0,
            rate: 1.0
        };
        this.aws = new AWSAuthentication();
    }

    _askForPermissions = async () => {
        const response = await Permissions.askAsync(Permissions.AUDIO_RECORDING);
        this.setState({
            haveRecordingPermissions: response.status === 'granted',
        });
    };

    componentDidMount() {
        this._askForPermissions();
    };

    _toggleRecording = async () => {
        this.setState({
            isLoading: true,
        });

        if (!this.state.isRecording){

            //start recording
            await Audio.setAudioModeAsync({
                allowsRecordingIOS: true,
                interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
                playsInSilentModeIOS: true,
                shouldDuckAndroid: true,
                interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
            });
            if (this.recording !== null) {
                this.recording.setOnRecordingStatusUpdate(null);
                this.recording = null;
            }

            const recording = new Audio.Recording();
            await recording.prepareToRecordAsync(
                QUALITY
            );
            recording.setOnRecordingStatusUpdate(this._updateScreenRecordingStarted);

            this.recording = recording;
            await this.recording.startAsync(); // Will call callback to update the screen.

        } else {

            //terminate recording
            await this.recording.stopAndUnloadAsync();
            this._uploadAudioToServer();
        }


        this.setState({
            isLoading: false,
        });

    };

    _uploadAudioToServer = async () => {
        const filePath = this.recording.getURI();
        const fileType = 'audio/'+ QUALITY[Platform.OS].extension.slice(-3);
        const fileName = filePath.split('/').pop();

        //Create a Presigned URL
        this.aws.getPreSignedUrl(
            fileName,
            undefined,
            undefined,
            (err, presignedUrl) => {
                if(err === null) {
                    //Upload File
                    const xhr = new XMLHttpRequest();
                    xhr.open('PUT', presignedUrl);
                    xhr.setRequestHeader('X-Amz-ACL', 'public-read');
                    xhr.setRequestHeader('Content-Type', fileType);
                    xhr.onreadystatechange = function() {
                        console.log(xhr.responseText);
                        if (xhr.readyState === 4) {
                            if (xhr.status === 200) {
                                // Successfully uploaded the file.
                                console.log('File Uploaded!')
                            } else {
                                // The file could not be uploaded.
                                console.log('File upload failed. :( ')
                            }
                        }
                    };
                    xhr.send({ uri: filePath, type: fileType, name: fileName });
                } else {
                    console.log(err)
                }
            }
        )
    };

    _updateScreenRecordingStarted = (status) => {
        console.log(status);
        if (status.canRecord) {
            this.setState({
                isRecording: status.isRecording,
                recordingDuration: status.durationMillis,
            });
        } else if (status.isDoneRecording) {
            this.setState({
                isRecording: false,
                recordingDuration: status.durationMillis,
            });
        }
    };

    _updateScreenRecordingFinished = () => {

    };

    _renderAppHeader = () => {
        return(
            <HeaderView>
                <StyledRoundButton2 onPress={() => {this.props.navigation.navigate('Settings')}} disabled={this.state.isLoading} radius={48}>
                    <Icon name="icon-gear" size={32} color={Colors.btnTextColor}/>
                </StyledRoundButton2>
                <StyledRoundButton2 onPress={() => {this.props.navigation.navigate('History')}} disabled={this.state.isLoading} radius={48}>
                    <Icon name="icon-archive" size={32} color={Colors.btnTextColor}/>
                </StyledRoundButton2>
            </HeaderView>
        )
    };

    _renderAuthorizedScreen = () => {
        return(
            <RecordingCanvas>
                <StyledRoundButton onPress={this._toggleRecording} disabled={this.state.isLoading} radius={72}>
                    <Icon name={ !this.state.isRecording ? 'icon-microphone' : 'icon-media-stop'} size={48} color={Colors.btnTextColor}/>
                </StyledRoundButton>
            </RecordingCanvas>
        )
    };

    _renderNeedsPermissionScreen = () => {
        return (
            <Text>Permission Needed</Text>
        )
    };

    render() {
        return (
            <MainView>
                { this._renderAppHeader() }
                {this.state.haveRecordingPermissions &&
                    this._renderAuthorizedScreen()
                }
                {!this.state.haveRecordingPermissions &&
                    this._renderNeedsPermissionScreen()
                }
            </MainView>
        );
    }
}